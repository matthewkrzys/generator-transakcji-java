FROM openjdk:8-jdk-alpine
RUN sudo wget -qO- https://toolbelt.heroku.com/install-ubuntu.sh | sh

VOLUME /tmp
ARG JAR_FILE
COPY ${JAR_FILE} app.jar
CMD java -Djava.security.egd=file:/dev/./urandom -Dserver.port=$PORT -jar /app.jar
