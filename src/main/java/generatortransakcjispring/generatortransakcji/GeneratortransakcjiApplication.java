package generatortransakcjispring.generatortransakcji;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GeneratortransakcjiApplication {

	public static void main(String[] args) {
		SpringApplication.run(GeneratortransakcjiApplication.class, args);
	}
}
/*
- docker images
    - docker container ls
    - docker build --build-arg JAR_FILE=build/libs/generator-transakcji-0.1.0.jar --tag registry.heroku.com/generator-transakcji/web .
    - docker images
    - docker container ls
    - docker login --username=matthewkrzys@gmail.com --password=$HEROKU_API_KEY registry.heroku.com
    - docker push registry.heroku.com/generator-transakcji/web:latest
 */