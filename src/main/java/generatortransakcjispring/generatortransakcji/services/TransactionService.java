package generatortransakcjispring.generatortransakcji.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import generatortransakcjispring.generatortransakcji.components.Parser;
import generatortransakcjispring.generatortransakcji.logic.ParserParams;
import generatortransakcjispring.generatortransakcji.model.DataTransaction;
import generatortransakcjispring.generatortransakcji.model.Params;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Slf4j
public class TransactionService {


    private ParserParams parserParams;
    private Parser parser;

    public TransactionService() {
        this.parserParams = new ParserParams();
        this.parser=new Parser();
    }


    public List<DataTransaction> createParamsData(String customersId,
                                                  String dateRange,
                                                  String itemsCount,
                                                  String itemsQuantity,
                                                  String eventsCount) {
        Params params = parserParams.createParamsFromData(customersId,
                                                            dateRange,
                                                            itemsCount,
                                                            itemsQuantity,
                                                            eventsCount);
        return parser.parseParamsAndGetListDataTransaction(params);
    }

    public String getYaml(String customersId,
                          String dateRange,
                          String itemsCount,
                          String itemsQuantity,
                          String eventsCount){
        String yaml = null;
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        try {
            yaml = mapper.writeValueAsString(createParamsData(customersId,
                    dateRange,
                    itemsCount,
                    itemsQuantity,
                    eventsCount));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return yaml;
    }
}
